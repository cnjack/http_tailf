package main

import (
	"flag"
	"net/http"
	"io/ioutil"
	"log"
	"encoding/json"
	"fmt"
)

var port = flag.Int64("p", 1234, "port")

func init()  {
	flag.Parse()
}
func main()  {
	router := http.NewServeMux()
	router.Handle("/", new(tailHandler))
	http.ListenAndServe(fmt.Sprintf(":%d", *port), router)
}

type tailHandler struct {}

func (h *tailHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	body := make(map[string]interface{})
	body["method"] = req.Method
	body["request_rui"] = req.RequestURI
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		body["req_body"] = ""
	} else {
		defer req.Body.Close()
		body["req_body"] = string(reqBody)
	}
	printBody, _ := json.Marshal(body)
	log.Println(string(printBody))
	w.Write([]byte(""))
}

